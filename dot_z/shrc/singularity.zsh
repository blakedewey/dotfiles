prompt_singularity() {
    if [ ! -z $SINGULARITY_NAME ]; then
        p9k_prompt_segment -t "$SINGULARITY_NAME"
    fi
}
typeset -A singularity_icons=(
  "*"      $'\uF17C')
typeset POWERLEVEL9K_SINGULARITY_VISUAL_IDENTIFIER_EXPANSION=$'${singularity_icons[(k)$PWD]:-\uF17C}'

